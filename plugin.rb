# name: Community Mobile SSO
# about: Get Payload Token for SSO Login from mobile app
# version: 0.3.1
# authors: Craig Kahle, Harold Sanchez Balaguera, Gustavo Scanferla
# url: https://bitbucket.org/amazingacademy/community-mobile-sso-plugin/

after_initialize do
  load File.expand_path("../app/controllers/amazing_controller.rb", __FILE__)

  SessionController.class_eval do

    skip_before_filter :check_xhr, only: ['sso', 'sso_login', 'become', 'sso_provider', 'mobile_return_nonce']

    def mobile_return_nonce
      nonce = DiscourseSingleSignOn.generate_url(params[:return_path] || '/')
      uri_array = Rack::Utils.parse_query(nonce)

      render json: uri_array
    end

  end

  Discourse::Application.routes.append do
    get "session/sso/mobile" => "session#mobile_return_nonce"
    get 'session/sso/mobile/login' => 'amazing#sso_mobile_login'
  end
end