class AmazingController < ApplicationController

  require_dependency 'single_sign_on'

  skip_before_filter :check_xhr, only: ['sso_mobile_login']
  skip_before_filter :redirect_to_login_if_required, only: ['sso_mobile_login']

  def sso_mobile_login

    puts "@@@@@ updated @@@@@"

    unless SiteSetting.enable_sso
      render nothing: true, status: 404
      return
    end

    sso = DiscourseSingleSignOn.parse(request.query_string)
    if !sso.nonce_valid?
      render text: I18n.t("sso.timeout_expired"), status: 500
      return
    end

    sso.expire_nonce!

    begin
      if user = sso.lookup_or_create_user
        if SiteSetting.must_approve_users? && !user.approved?
          render text: I18n.t("sso.account_not_approved"), status: 403
        else
          Discourse::Application.config.session_store :cookie_store, key: '_forum_session'
          log_on_user user

          render json: user
        end
      else
        render text: I18n.t("sso.not_found"), status: 500
      end
    rescue => e
      details = {}
      SingleSignOn::ACCESSORS.each do |a|
        details[a] = sso.send(a)
      end
      Discourse.handle_exception(e, details)

      render text: I18n.t("sso.unknown_error"), status: 500
    end
  end


end